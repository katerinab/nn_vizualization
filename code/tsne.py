import numpy as np
import matplotlib.pyplot as plt
from time import time
from PIL import Image
import os

from keras.datasets import mnist

start = time()
DEBUG_VERSION = False
n = 600


def scale(data):
    data = data[:, np.std(data, axis=0) != 0]
    return np.nan_to_num((data - np.mean(data, axis=0)) / np.std(data, axis=0), 0)


def squared_distance(X):
    sum_X = np.sum(np.square(X), 1, keepdims=True)
    return (sum_X + sum_X.T - 2*np.dot(X, X.T)).clip(min=0)


def cal_p(squared_d, sigma, idx):
    probability = np.exp(-squared_d/(2 * sigma ** 2))
    probability[idx] = 1
    s = sum(probability)
    probability /= s - 1
    probability[idx] = 1
    prplx = 2**(-np.sum(probability[probability != 0] * np.log2(probability[probability != 0])))
    probability[idx] = 0
    return probability, prplx


def find_sigma(x, idx, prplx):
    smin, smax = 0, np.inf
    sigma = np.ones(n)
    probs, prplx2 = cal_p(x, sigma[idx], idx)
    diff = prplx - prplx2
    counter = 0
    hit_limit = False
    while counter < 100 and abs(diff) > .01:
        if diff > 0:
            if not hit_limit:
                smin = sigma[idx]
                smax = 2 * smin
                sigma[idx] = smax
            else:
                smin = sigma[idx]
                sigma[idx] = (smin + smax) / 2
        else:
            smax = sigma[idx]
            sigma[idx] = (smin + smax) / 2
            hit_limit = True
        probs, prplx2 = cal_p(x, sigma[idx], idx)
        diff = prplx - prplx2
        counter += 1
    return probs


def compute_probabilities(x, prplx):
    n = x.shape[0]
    dist = squared_distance(x)
    dist /= np.std(dist, axis=1)

    pw_probs = np.zeros([n, n])
    for i in range(n):
        d = dist[i]
        prob = find_sigma(d, i, prplx)
        pw_probs[i] = prob
        if i % 1 == 0 and DEBUG_VERSION:
            print(f'\rpca: {i}/{n}', end='')
    if DEBUG_VERSION:
        print()
    return pw_probs


def pca(data):
    vals, vectors = np.linalg.eig(np.dot(data.T, data))
    vals_norm = vals/np.sum(vals)
    p_sum = 0
    cut = 0
    while p_sum < 0.75 and cut < vals_norm.shape[0]:
        p_sum += vals_norm[cut]
        cut += 1
    return np.dot(data, vectors[:, 0:cut])


def tsne(probs, lr, mom, iterations):
    if DEBUG_VERSION:
        print("TSNE")
    dims = 2
    proj = np.random.normal(scale=0.01, size=(n, dims))
    # projections = []
    v = 0
    for i in range(iterations):
        distances = squared_distance(proj)
        q = 1 / (1 + distances)
        np.fill_diagonal(q, 0)
        Q = q / np.sum(q, axis=1, keepdims=True)
        d = proj.T.reshape(2, n, 1) - proj.T.reshape(2, 1, n)

        if (i + 1) % 10 == 0 and DEBUG_VERSION:
            print(f"\r{i+1}/{iterations}")
            # projections.append(proj.copy())

        grad = np.sum(4 * (probs - Q) * d * q, axis=2)

        v = mom * v + lr * grad.T
        proj -= v
    return proj


def main():
    proj_path = 'data/projection/'
    if not os.path.exists(proj_path):
        os.makedirs(proj_path)
    if DEBUG_VERSION:
        plot_path = 'plots/'
        if not os.path.exists(plot_path):
            os.makedirs(plot_path)

    # layer_names = ['l0_dense', 'l1_relu', 'l3_dense', 'l4_relu', 'l6_dense', 'l7_soft']
    layer_names = ['l0_dense', 'l1_relu', 'l3_dense', 'l4_relu', 'l6_dense', 'l7_soft']
    y = np.load('data/outputs_of_layers/y.npy', allow_pickle=True)
    np.random.seed(7)
    idxs = np.random.choice(range(y.shape[0]), min(n, y.shape[0]))
    y = y[idxs]
    np.save(f'{proj_path}y.npy', y)
    np.save(f'{proj_path}idxs.npy', idxs)
    _, (images, _) = mnist.load_data()
    images = images[idxs]
    output_path = 'images/'
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    for im_arr, i in zip(images, range(images.shape[0])):
        im = Image.fromarray(im_arr)
        im.save(output_path + f'x_{i}.png')


    perplexity = 80
    learning_rate = 0.3
    momentum = 0.4

    for ln in layer_names:
        X = np.load(f'data/outputs_of_layers/{ln}.npy', allow_pickle=True)[idxs]

        probs = compute_probabilities(pca(scale(X)), perplexity)

        # np.save(f'data/prob_{ln}_prplx{perplexity:03}.npy', probs)
        # P = np.load('p.npy', allow_pickle=True)

        assert not np.any(np.isnan(probs))

        probs = (probs + np.transpose(probs)) / 2

        max_iter = 150

        projection = tsne(probs, learning_rate, momentum, max_iter)
        np.save(f'{proj_path}{ln}.npy', projection)

        if DEBUG_VERSION:
            plt.figure(figsize=(12, 10))
            plt.scatter(projection[:, 0], projection[:, 1], c=y.astype(int), cmap='tab10', s=12)
            plt.colorbar()
            plt.suptitle(f"{ln} - prplx: {perplexity}  lr: {learning_rate}  mom: {momentum}")
            plt.savefig(f'{plot_path}{ln}_prplx{perplexity:03}_lr{learning_rate}_mom{momentum}.png')
            plt.show()

        if DEBUG_VERSION:
            print(f"\n\nTime: {time() - start:.3f} s\n\n")

if __name__ == '__main__':
    main()