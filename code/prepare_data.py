import tsne, train_NN, generate_activation_matrix

if __name__ == '__main__':
	print("Training the neural network on MNIST")
	train_NN.main()
	print('Calculating TSNe projection')
	tsne.main()
	print('Exporting activation matrix')
	generate_activation_matrix.main()