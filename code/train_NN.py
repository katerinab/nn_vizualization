import numpy as np
import matplotlib.pyplot as plt
import os

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.utils import np_utils
from keras.utils.vis_utils import plot_model
import keras
import pydot as pyd
from IPython.display import SVG
from keras.utils.vis_utils import model_to_dot
import tensorflow as tf
tf.compat.v1.disable_eager_execution()

keras.utils.vis_utils.pydot = pyd

plt.rcParams['figure.figsize'] = (7, 7)  # Make the figures a bit bigger

# the data, shuffled and split between tran and test sets
(X_train, y_train), (X_test, y_test) = mnist.load_data()
print("X_train original shape", X_train.shape)
print("y_train original shape", y_train.shape)

nb_classes = 10

# for i in range(9):
#     plt.subplot(3, 3, i + 1)
#     plt.imshow(X_train[i], cmap='gray', interpolation='none')
#     plt.title("Class {}".format(y_train[i]))
# plt.show()

X_train = X_train.reshape(60000, 784)
X_test = X_test.reshape(10000, 784)
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255
print("Training matrix shape", X_train.shape)
print("Testing matrix shape", X_test.shape)

y_train = np_utils.to_categorical(y_train, nb_classes)
y = y_test.copy()
y_test = np_utils.to_categorical(y_test, nb_classes)


def visualize_model(model):
    return SVG(model_to_dot(model).create(prog='dot', format='svg'))


def learn_and_save_model():
    global X_train, X_test, y_train, y_test

    model = Sequential()
    model.add(Dense(512, input_shape=(784,)))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    model.add(Dense(10))
    model.add(Activation('softmax'))  # This special "softmax" activation among other things,
    # ensures the output is a valid probaility distribution, that is
    # that its values are all non-negative and sum to 1.

    model.compile(loss='categorical_crossentropy', optimizer='adam')

    model.fit(X_train, y_train,
              batch_size=128, epochs=4,
              verbose=1,
              validation_data=(X_test, y_test))

    print(model.summary())
    visualize_model(model)

    plot_model(model, to_file='model.png')

    # model.save('model_w_dropout.h5')

    return model

def main():
    model = learn_and_save_model()
    # model = load_model('model.h5')

    model.summary()
    # plot_model(model, to_file='model.png', show_shapes=True, show_layer_names=False)

    score = model.evaluate(X_test, y_test, verbose=0)
    print('Test score:', score)

    extractor = keras.Model(inputs=model.inputs,
                            outputs=[layer.output for layer in model.layers])
    session = tf.compat.v1.keras.backend.get_session()
    features = session.run(extractor(tf.convert_to_tensor(X_test)))

    layer_names = ['l0_dense', 'l1_relu', 'l2_drop', 'l3_dense', 'l4_relu', 'l5_drop', 'l6_dense', 'l7_soft']

    newpath = 'data/outputs_of_layers/'
    print(f'Saving the data to ./{newpath}')
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    for i, ln in zip(range(len(layer_names)), layer_names):
        np.save(f'{newpath}{ln}.npy'.format(ln), features[i])

    np.save(f'{newpath}y.npy', y)
    prediction = model.predict(X_test)
    if not os.path.exists('data/projection/'):
        os.makedirs('data/projection/')
    np.save(f'data/projection/prediction.npy', prediction.argmax(axis=1))


if __name__ == '__main__':
    main()

