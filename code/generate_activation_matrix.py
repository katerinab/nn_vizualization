import os
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


def main():
    path = 'data/outputs_of_layers/'
    output_path = 'data/activations/'
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    layer = 'l4_relu'
    n_neurons = 20
    PLOT = False

    layer_outputs = np.load(f'{path}{layer}.npy', allow_pickle=True)
    y = np.load(f'{path}y.npy')

    mean_activations_by_classes = np.zeros([10, layer_outputs.shape[1]])
    for i in range(10):
        mean_activations_by_classes[i, :] = np.mean(layer_outputs[y == i], axis=0)
    variances_of_neurons = np.var(mean_activations_by_classes, axis=0)
    idxs = np.argsort(-variances_of_neurons)

    mean_activations_by_classes = mean_activations_by_classes[:, idxs[:n_neurons]]
    layer_outputs = layer_outputs[:, idxs[:n_neurons]]

    m = np.max(layer_outputs, axis=0)
    layer_outputs = layer_outputs / m
    mean_activations_by_classes = mean_activations_by_classes / m

    if PLOT:
        n_samples = 5
        plt.figure(figsize=(16, 8))
        sns.heatmap(np.concatenate([mean_activations_by_classes, np.zeros((1, n_neurons)), layer_outputs[:n_samples, :]],
                                   axis=0),
                    yticklabels=[f'class {i}' for i in range(10)]+['']+[f'sample {i}' for i in list(y[:n_samples])],
                    cmap="YlGnBu", annot=False, vmin=0, vmax=1)
        plt.xlabel('neurons')
        plt.ylabel('classes')
        plt.show()

    np.save(f'{output_path}{layer}_per_class.npy', mean_activations_by_classes)
    np.save(f'{output_path}{layer}_per_sample.npy', layer_outputs)
    np.save(f'{output_path}y.npy', y)

if __name__ == '__main__':
   main()
