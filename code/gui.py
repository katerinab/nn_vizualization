import PySimpleGUI as sg
import numpy as np

# maybe future work? -> set dynamically resizable elements
# screen_width, screen_height = sg.Window.get_screen_size()
# screen_width = int(0.9*screen_width)
# screen_height = int(0.9*screen_height)
screen_width, screen_height = (1800, 950)
scale_data = 100  # for better precession on click

# Size related constants
scatter_canvas_size = np.floor((0.84 * screen_width) / 3)
legend_height = scatter_canvas_size
legend_width = np.ceil(0.1 * screen_width / 3)
top_elements_pad = ((screen_width - legend_width - scatter_canvas_size * 3) / 10,
                    5)
arch_width = 0.17 * screen_width
arch_height = 0.95 * (screen_height - scatter_canvas_size - 4 * top_elements_pad[0])
table_item_size = (9, 1)
max_num_selected_samples = 3

# fonts, themes, colors
large_font = 'Helvetica 18'
medium_font = 'Helvetica 14'
small_font = 'Helvetica 10'
colors = ['light coral', 'light salmon', 'light goldenrod', 'light yellow', 'aquamarine', 'light green',
          'light sea green', 'light steel blue', 'light pink', 'violet']
sg.theme('Dark Blue 14')

# element to id mapping
mapping_to_id = {}


def compute_color_scale():
    a, b = np.array([int('99b3e6'[i:i + 2], 16) for i in (0, 2, 4)]), \
           np.array([int('000066'[i:i + 2], 16) for i in (0, 2, 4)])
    colors = np.zeros((101, 3))
    step = (b - a) / 100
    for i in range(101):
        colors[i, :] = a + i * step
    rgb2hex = lambda r, g, b: '#%02x%02x%02x' % (r, g, b)
    return [rgb2hex(*colors.astype(int)[i, :]) for i in range(colors.shape[0])]


def map_cell_to_color(ms, sample_id, neuron_id):
    """
    :param ms: 2D array of integers from range [0, 100]...corresponds to scaled activations
    :param sample_id: first index to ms
    :param neuron_id: second index to ms
    :return: string representation of color mapping, can be in HEX format or python defined color
    """
    # return f'grey{ms[sample_id, neuron_id]}'
    return color_scale[ms[sample_id, neuron_id]]  # .get_hex_l()


def get_sample_color(y, y_pred, sample_id):
    return 'green' if y[sample_id] == y_pred[sample_id] else 'red'


def draw_scatter(X, y, scale, graph, title, sq_size):
    pos = (0, sq_size * scale_data - sq_size * scale_data / 10)
    for point, label in zip(X, y):
        point = point * scale_data
        graph.DrawPoint((point[0], point[1]), 0.7 * scale, color=colors[label])
    graph.DrawText(title, pos, font=large_font)


def draw_architecture(graph):
    titles = [
        '0. Input Layer (in:784 out:784)',
        '1. Dense Layer (in:784 out:512)',
        '2. ReLU Activation (in:784 out:512)',
        '3. Dropout Layer (in:512 out:512)',
        '4. Dense Layer (in:512 out:512)',
        '5. ReLU Activation (in:512 out:512)',
        '6. Dropout Layer (in:512 out:512)',
        '7. Dense Layer (in:512 out:10)',
        '8. SoftMax Activation (in:10 out:10)'
    ]
    pos = (0, 1)
    graph.DrawText('Architecture', pos, font=medium_font)
    for i in range(len(titles)):
        pos = (0, 3 + 2 * i)
        graph.DrawText(titles[i], pos, font=small_font)


def get_datasets():
    X0 = np.load(f'data/projection/l0_dense.npy', allow_pickle=True)
    X1 = np.load(f'data/projection/l1_relu.npy', allow_pickle=True)
    X2 = np.load(f'data/projection/l6_dense.npy', allow_pickle=True)
    y = np.load('data/projection/y.npy', allow_pickle=True)
    y_pred = np.load('data/projection/prediction.npy', allow_pickle=True)
    idxs = np.load('data/projection/idxs.npy', allow_pickle=True)
    mc = np.load(f'data/activations/l4_relu_per_class.npy', allow_pickle=True)
    ms = np.load(f'data/activations/l4_relu_per_sample.npy', allow_pickle=True)[idxs]
    y_pred = y_pred[idxs]
    return X0, X1, X2, y, y_pred, mc, ms


def get_img_location(num_selected):
    img_loc = (-sq_size * scale_data + (
            (max_num_selected_samples + num_selected) * 2 * sq_size * scale_data / (3 * max_num_selected_samples)),
               -7 * sq_size * scale_data / 8)
    return img_loc


def perform_selection(event, values, graphs, window, labels, selection_list, ms, X0, X1, X2):
    (x, y) = values[event]
    drag_figures = list(graphs[event].get_figures_at_location((x, y)))
    drag_figures = list(filter(lambda x: x <= 600, drag_figures))
    if len(drag_figures) != 0:
        selected = drag_figures[0]
        if selected in selection_list:
            id_in_list = selection_list.index(selected)
            remove_selection(X0, X1, X2, graphs, id_in_list, labels, selection_list, window, labels, y_pred)
        else:
            if len(selection_list) < max_num_selected_samples:
                for graph, X in zip(graphs.values(), [X0, X1, X2]):
                    graph.TKCanvas.itemconfig(selected, fill='red')
                    graph.bring_figure_to_front(selected)
                    img_loc = get_img_location(len(selection_list))
                    img = graph.DrawImage(filename=f'images/x_{selected - 1}.png', location=img_loc)
                    from_id = (scale_data * X[selected - 1][0], scale_data * X[selected - 1][1])
                    line = graph.DrawLine(from_id, img_loc, color="black", width=1)
                    mapping_to_id[len(selection_list)] = (img, line)
                row = len(selection_list)
                for col in range(20):
                    cell_id = f'b_{row + 10}_{col}'
                    cell = window[cell_id]
                    cell.Update(background_color=map_cell_to_color(ms, selected - 1, col))
                cell_id = f't_{row + 10}'
                cell = window[cell_id]
                cell.Update(f'{len(selection_list) + 1}.y={labels[selected - 1]} y\'={y_pred[selected - 1]}',
                            background_color=get_sample_color(labels, y_pred, selected - 1))
                selection_list += [selected]
            else:
                sg.popup_error('Error',
                               'Cannot select more than three data samples.\nPlease, reset the selection or remove some of the selected samples by clicking on them.\n\nPress button or ENTER to exit.',
                               font=medium_font)


def remove_selection(X0, X1, X2, graphs, id_in_list, labels, selection_list, window, y, y_pred):
    selected = selection_list[id_in_list]
    for graph in graphs.values():
        graph.TKCanvas.itemconfig(selected, fill=colors[labels[selected - 1]])
        graph.DeleteFigure(mapping_to_id[id_in_list][0])
        graph.DeleteFigure(mapping_to_id[id_in_list][1])
    clear_table_sample(id_in_list, selected, selection_list, window, [X0, X1, X2], y, y_pred)


def clear_table_sample(id_in_list, selected, selection_list, window, X_list, y, y_pred):
    for col in range(20):
        cell_id = f'b_{id_in_list + 10}_{col}'
        window[cell_id].Update(background_color='grey')
    cell_id = f't_{id_in_list + 10}'
    window[cell_id].Update(f'sample x', background_color=sg.theme_element_background_color())
    for i in range(id_in_list + 1, len(selection_list)):
        for col in range(20):
            cell_id = f'b_{i - 1 + 10}_{col}'
            cell = window[cell_id]
            cell.Update(background_color=map_cell_to_color(ms, selection_list[i] - 1, col))
        cell_id = f't_{i - 1 + 10}'
        cell = window[cell_id]
        cell.Update(f'{i}.y={y[selection_list[i] - 1]} y\'={y_pred[selection_list[i] - 1]}',
                    background_color=get_sample_color(y, y_pred, selection_list[i] - 1))
        for col in range(20):
            cell_id = f'b_{i + 10}_{col}'
            cell = window[cell_id]
            cell.Update(background_color=f'grey')
        cell_id = f't_{i + 10}'
        cell = window[cell_id]
        cell.Update(f'sample x', background_color=sg.theme_background_color())
        for graph, X in zip(graphs.values(), X_list):
            img_loc = get_img_location(i - 1)
            img = graph.DrawImage(filename=f'images/x_{selection_list[i] - 1}.png', location=img_loc)
            from_id = (scale_data * X[selection_list[i] - 1][0], scale_data * X[selection_list[i] - 1][1])
            line = graph.DrawLine(from_id, img_loc, color="black", width=1)
            mapping_to_id[i - 1] = (img, line)
            graph.DeleteFigure(mapping_to_id[i][0])
            graph.DeleteFigure(mapping_to_id[i][1])
    selection_list.remove(selected)


def normalize_to_100(mc, ms):
    mc = np.ceil(100 * (mc / np.max(np.abs(mc)))).astype(int)
    ms = np.ceil(100 * (ms / np.max(np.abs(ms)))).astype(int)
    return mc, ms


def draw_legend(legend):
    step_size = legend_height / 10
    for i in range(10):
        loc = (legend_width / 2, legend_height - step_size / 2 - step_size * i)
        id1 = legend.DrawPoint(loc, 30, color=colors[i])
        id2 = legend.DrawText(str(i), loc, color="black", font=medium_font)


if __name__ == "__main__":
    # color_scale = list(Color("#99b3e6").range_to(Color("#000066"), 101))
    color_scale = compute_color_scale()

    selection_list = []
    X0, X1, X2, y, y_pred, mc, ms = get_datasets()
    mc, ms = normalize_to_100(mc, ms)

    sq_size = np.ceil(1.01 * np.max(
        np.array([np.abs(np.min(X0)), np.max(X0), np.abs(np.min(X1)), np.max(X1), np.abs(np.min(X2)), np.max(X2)])))
    fig_args = {'graph_bottom_left': (-sq_size * scale_data, -sq_size * scale_data),
                'graph_top_right': (sq_size * scale_data, sq_size * scale_data),
                'background_color': 'grey',
                'change_submits': True,
                'pad': top_elements_pad,
                'enable_events': True,
                'canvas_size': (scatter_canvas_size, scatter_canvas_size)}

    # TABLE INIT
    headings = [' '] + ['neuron ' + str(i + 1) for i in range(20)]
    header = [[sg.T(h, size=table_item_size, pad=(0, 0), key=f't_{h}', auto_size_text=False, font=small_font) for h in
               headings]]
    table_classes = [[sg.T(f'class {row}', size=table_item_size, pad=(0, 0), key=f't_{row}', auto_size_text=False,
                           font=small_font)] + [
                         sg.T('', size=table_item_size, auto_size_text=False, font=small_font,
                              background_color=map_cell_to_color(mc, row, col), pad=(0, 0),
                              key=f'b_{row}_{col}') for col
                         in range(20)] for row in range(10)]
    table_samples = [[sg.T(f'sample x', size=table_item_size, auto_size_text=False, pad=(0, 0), key=f't_{row + 10}',
                           font=small_font)] + [
                         sg.T('', auto_size_text=False, size=table_item_size, background_color=('grey'), pad=(0, 0),
                              key=f'b_{row + 10}_{col}',
                              font=small_font) for col in
                         range(20)] for row in range(max_num_selected_samples)]
    separator1 = [[sg.Text(
        f'Average activations of significant neurons for all classes (the darker cell, the higher activation of neuron)',
        auto_size_text=True, font=medium_font, pad=top_elements_pad),
        sg.Button('Info', font=medium_font)]]
    separator2 = [[sg.Text(f'Average activations of significant neurons for selected samples', auto_size_text=False,
                           font=medium_font, pad=top_elements_pad),
                   sg.Button('Reset selection (click or press R)', font=medium_font)]]
    bottom_left = separator1 + header + table_classes + separator2 + table_samples

    bottom_right = sg.Graph(key='architecture',
                            graph_bottom_left=(-1, 20),
                            graph_top_right=(1, 0),
                            background_color='grey', change_submits=True,
                            canvas_size=(arch_width, arch_height),
                            pad=(40, 40)
                            )

    # Layout init
    layout = [
        [
            sg.Graph(key='graph0', **fig_args),
            sg.Graph(key='graph1', **fig_args),
            sg.Graph(key='graph2', **fig_args),
            sg.Graph(key='legend', background_color='grey',
                     canvas_size=(legend_width, legend_height),
                     graph_top_right=(legend_width, legend_height),
                     graph_bottom_left=(0, 0), pad=top_elements_pad)
        ],
        [sg.Column(bottom_left, pad=(0, 0)), bottom_right]
    ]

    window = sg.Window('MNIST neural network visualization', layout, resizable=False,
                       size=(screen_width, screen_height), return_keyboard_events=True)
    window.Finalize()
    window.maximize()

    graphs = {
        'graph0': window['graph0'],
        'graph1': window['graph1'],
        'graph2': window['graph2']
    }

    # Draw on canvas
    draw_scatter(X0, y, scale_data, window['graph0'], '1. Dense layer', sq_size)
    draw_scatter(X1, y, scale_data, window['graph1'], '2. ReLU layer', sq_size)
    draw_scatter(X2, y, scale_data, window['graph2'], '7. Dense layer', sq_size)
    draw_architecture(window['architecture'])
    draw_legend(window['legend'])

    # Read events
    while True:
        event, values = window.read()
        if event is None:
            break
        if event in graphs.keys():
            perform_selection(event, values, graphs, window, y, selection_list, ms, X0, X1, X2)
        if event in ['Reset selection (click or press R)', 'r', 'r:27']:  # if user closes window or clicks cancel
            for id_in_list in range(len(selection_list) - 1, -1, -1):
                remove_selection(X0, X1, X2, graphs, id_in_list, y, selection_list, window, y, y_pred)
        if event == 'Info':  # if user closes window or clicks cancel
            with open('info_text', mode='r') as f:
                info_text = f.read()
            sg.popup('INFO', info_text)
    window.close()
