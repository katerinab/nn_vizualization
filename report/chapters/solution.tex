\section{Solution} \label{sec:solution}

This section describes the t-SNE implementation and the visualization. Furthermore, it identifies the possible viewpoints of the user and the insights that the visualization can give him/her. We thoroughly describe all of the components and the connection between them.

\subsection{t-distributed stochastic neighbor embedding}

As a part of this project, we have implemented t-SNE algorithm for visualizing the samples. We implemented scripts that extract the outputs of all layers of the neural network for all the samples. The data consist of a n-dimensional vector for each sample depending on the layer. t-SNE tranforms the data into two dimensions so that the vectors that are similar are closer to each other and the distinct vectors lay far from each other. t-SNE does so by computing a probability distribution that describes the relations between the samples. More precisely it computes conditional probabilities $p_{j|i}$ that sample $x_j$ would be chosen as neighbor of $x_i$, if neighbors would be chosen by sampling from normal distribution centered in $x_i$. Afterwards the algorithm tries to recreate those probabilities in space with fewer dimensions (usually 2) using t-Student's t-distribution and gradient descent. In our case the output of t-SNE are two dimensional points - the projection. 

t-SNE accepts a few arguments, such as perplexity, learning rate and momentum. In order to utilize this algorithm the most, we ran parameter tuning on the t-SNE and used the settings that gave the best results.

In Figure \ref{fig:l0} and \ref{fig:l6}, we present the computed projections for the first layer and for the second to last layer. Both of them are dense layers. 

\begin{figure}[H]
\centering
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{fig/l0.png}
    \caption{First layer of the network}
    \label{fig:l0}
\end{subfigure}%
\hfill
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{fig/l6.png}
    \caption{Second to last layer of the network}
    \label{fig:l6}
\end{subfigure}
\caption{Sample t-SNE projections}
\end{figure}


\subsection{GUI}

We made an interactive visualization with two main components where a selection action in one affects the content of the other. We are working with a fully trained neural network for MNIST dataset and t-SNE projection. More details about the data we use can be found in chapter (\ref{sec:analysis}).

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{fig/screenshot.png}
    \caption{Screenshot of the proposed GUI}
    \label{fig:GUI}
\end{figure}

\subsubsection{The top part of the screen}

Screenshot of the graphical interface can be seen in Figure \ref{fig:GUI}. In the \textbf{top} part of the screen, we can see scatter plots visualizing the way the neural network separated the images into different clusters. Each point in the plot represents one sample from the dataset (image of some digit).

Its location is given by a feature vector of the sample in a given layer that is projected using t-SNE. We use ten easily distinguishable python defined colours to show the true label (class) of each data point.

We selected the categorical color mapping so that the colors are easily distinguishable, which allows the user to easily identify the clusters. Moreover, we selected light colors so that the selection (bright red point) is quickly identifiable in all of the plots. The class to color assignment is 'rainbow-like' to provide at least some intuition, but we add a color to the class legend for more convenience.

The user is able to click any of the data points (in any of the scatter plots). This data point is then highlighted by a bright red color in all of the scatter plots. This allows the user to peak into the clusterization process.

As soon as the data point is highlighted, the image representation is shown at the bottom of the plot. This allows the user to identify why some of the samples are not classified correctly. In Figure \ref{fig:scatter}, we selected two very close data points that do not have the same color and now, we can compare their images (we also see more info about the selection in the bottom part of the screen). In the provided example, we can see that 3 was incorrectly classified as 7, and that the reason might be that the top part of the image (that almost corresponds to 7) is thicker. This might convince the neural network that the bottom part is insignificant, and classify it as seven based on the top part of the image.

Currently, it is allowed to select three data point which should be sufficient for detailed comparison but low enough to prevent information clutter. It is possible to deselect the data point by clicking on it again, but as it might be a bit tricky, we also added a reset button and reset keyboard event for more comfortable interaction.


\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{fig/scatter.png}
    \caption{Screenshot of the t-SNE scatter plots}
    \label{fig:scatter}
\end{figure}



\subsubsection{The bottom part of the screen}

The \textbf{bottom right window} contains the architecture of the network. Even though this is not an interactive element, it gives the user a better context about the network. The user can see, which type of the neural network layers are used, identify their order, and match them with the visualized data. However, this element is not as thoroughly designed and is aimed mainly on the developers. Otherwise, it would be necessary to design a node-link diagram to better visualize the architecture.

The \textbf{bottom left window} contains a matrix view of the neuron activations in one of the layers. The layer was chosen randomly from the middle of the network. Afterwards, we chose 20 neurons from this layer with largest variance of activations amongst classes in order to maximize the visible differences.

Each column corresponds to one neuron, and each row corresponds to one data sample or a subset of data samples corresponding to one class. When the whole class is chosen, the activations are calculated as the mean value of all corresponding samples. In the default view, the user will see ten predefined subsets (each represents samples of one class).

Moreover, when a point (data sample) is selected in the top window, a new row corresponding to its activations appears. The additional rows corresponding to the user's selection are marked by green or red color to show whether the sample was correctly or incorrectly classified respectively. The row label also indicates the true ($y$) and predicted ($y'$) class of the sample.

You can see the sample usage in Figure \ref{fig:act}. The first selected data point is a four from the middle of the cluster, i.e. very confidently classified four. The second selected data point is a six that was far away from the cluster of digits six. In the matrix view, we can see that the six was actually identified as a one. The interpretation of the class $i$ rows should more or less correspond to the perfect activation values for the given digit $i$. We can see that the 'perfectly' selected sample of four pretty much corresponds to the average activation values for digit four. On the other side, we can see the struggle of the second sample that is missing the strong activations of several neurons corresponding to class six. Moreover, we can see that all of the activations are very low for this sample. It is quite possible that this sample activated other neurons that are not present in the visualization (only 20 most significant out of 512 neurons are shown), and that the best corresponding class was six (which is not really visible from the shown activations). This basically means that the neural network was not prepared for such sample, and if that was possible, it might put it to some "class 10".


\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{fig/activations.png}
    \caption{Screenshot of the NN activations from layer 5. (ReLU)}
    \label{fig:act}
\end{figure}

The color is selected from a sequential color map. Firstly, the activations are scaled (and rounded to integers) to interval 0-100. We selected two shades of blue -- first with low saturation and hue, second with high saturation and hue. We used the RGB color model represented in hexadecimal form and interpolated between these to colors. The result of the interpolation is a 101-items long sequential color map that converts the intensity of the blue color to the value of the given activation. We decided to represent lowest activation as the lightest color and the largest activation as the darkest color. I.e., the darker the color is, the higher the activation is. To reduce the GUI clutter, we did not insert any legend but rather gave a short text description as the matrix title.

The \textbf{connection between the GUI parts} is ensured mainly by the possibility to select the data samples. The interaction enables the user to peek into a very large dataset that could not possibly be viewed as a whole. Moreover, the interaction possibilities are focused on the border cases, which are often exactly the kind of data samples where the user can intuitively understand WHY and HOW something works. 

The main visual channels that we used are color, shape and position (categorical scatter plot), and lightness (sequential mapping in the matrix of activations). One may say that we might use a different labelling procedure (of digit images to scatter plot) that would be more visually pleasing, but the current labelling makes sense in the context of the order of the selection that is very easy to identify. This allows the user to easily not only to connect the selected sample point with the image but also to connect the image with the activation matrix row (based on the selection order).



