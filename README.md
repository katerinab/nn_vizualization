# Visualization of neural network for MNIST dataset

NN visualization project shows different ways how to visualize training of the neural network on MNIST dataset. It contains NN and TSNe implementation and custom NN visualization. Also contains a report with a state-of-the-art overview and a more detailed description of the visualization process.

The visualization consists of TSNe projection of selected NN layers and comparison of activations of significant neurons. It is possible to interact with the plots and compare the images and activations of selected samples from MNIST dataset.

The project is a semestral work for CTU subject Visualization in SS 2019/20.

![GitHub Logo](screenshot.png)


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Use python virtual environment and install requirements from requirements.txt. It may be necessary to additionally install Graphviz to your system. Python version>=3.7 is recommended.

```
ipython>=7.14.0
Keras>=2.3.1
numpy>=1.17.4
pydot>=1.4.1
tensorflow>=2.2.0
matplotlib>=3.2.1
graphviz>=0.14
pysimplegui>=4.19.0
seaborn>=0.10.0
pillow>=7.1.2
colour~=0.1.5
```

### Running the project

Run `prepare_data.py` to generate all necessary data for the visualization tool; the data will be saved to folder `data/`. You can also run the scripts `train_NN.py`, `tsne.py` and `generate_activation_matrix.py` as stand-alone scripts. After the data is ready, run `gui.py` to see the visualized network.



## Authors

* **Kateřina Brejchová**
* **Erik Vaknin**

Supervisor: Ladislav Čmolík

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details



